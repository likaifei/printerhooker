﻿/*
 * 由SharpDevelop创建。
 * 用户： PC
 * 日期: 2018/01/14
 * 时间: 13:36
 * 
 * 要改变这种模板请点击 工具|选项|代码编写|编辑标准头文件
 */
namespace printerhook
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnopen = new System.Windows.Forms.Button();
			this.cbSerial = new System.Windows.Forms.ComboBox();
			this.t_recv = new System.Windows.Forms.TextBox();
			this.cbSerialOut = new System.Windows.Forms.ComboBox();
			this.btnOut = new System.Windows.Forms.Button();
			this.t_recvhex = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// btnopen
			// 
			this.btnopen.Location = new System.Drawing.Point(12, 59);
			this.btnopen.Name = "btnopen";
			this.btnopen.Size = new System.Drawing.Size(67, 44);
			this.btnopen.TabIndex = 0;
			this.btnopen.Text = "打开";
			this.btnopen.UseVisualStyleBackColor = true;
			this.btnopen.Click += new System.EventHandler(this.BtnopenClick);
			// 
			// cbSerial
			// 
			this.cbSerial.FormattingEnabled = true;
			this.cbSerial.Location = new System.Drawing.Point(10, 13);
			this.cbSerial.Name = "cbSerial";
			this.cbSerial.Size = new System.Drawing.Size(94, 20);
			this.cbSerial.TabIndex = 1;
			// 
			// t_recv
			// 
			this.t_recv.Location = new System.Drawing.Point(138, 38);
			this.t_recv.Multiline = true;
			this.t_recv.Name = "t_recv";
			this.t_recv.Size = new System.Drawing.Size(225, 338);
			this.t_recv.TabIndex = 2;
			this.t_recv.Text = "结帐清单古茗茶饮PRATO\n店\n项目   数量   单价  小计结帐时间:2018-02-16 01:40\n103001\n1店号\n机器\n人数单号\n桌号021600" +
			"02\n1.0000\n4.504.501椰果奶茶（大）\n4.504.501椰果奶茶（大）\n4.50总计\n 操作员:0001服务员:0001\n.0.00找零4.50" +
			"\n4.50应收\n现金";
			// 
			// cbSerialOut
			// 
			this.cbSerialOut.FormattingEnabled = true;
			this.cbSerialOut.Location = new System.Drawing.Point(138, 12);
			this.cbSerialOut.Name = "cbSerialOut";
			this.cbSerialOut.Size = new System.Drawing.Size(94, 20);
			this.cbSerialOut.TabIndex = 3;
			// 
			// btnOut
			// 
			this.btnOut.Location = new System.Drawing.Point(12, 129);
			this.btnOut.Name = "btnOut";
			this.btnOut.Size = new System.Drawing.Size(67, 44);
			this.btnOut.TabIndex = 4;
			this.btnOut.Text = "发送";
			this.btnOut.UseVisualStyleBackColor = true;
			this.btnOut.Click += new System.EventHandler(this.BtnOutClick);
			// 
			// t_recvhex
			// 
			this.t_recvhex.Location = new System.Drawing.Point(396, 38);
			this.t_recvhex.Multiline = true;
			this.t_recvhex.Name = "t_recvhex";
			this.t_recvhex.Size = new System.Drawing.Size(225, 338);
			this.t_recvhex.TabIndex = 5;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(634, 400);
			this.Controls.Add(this.t_recvhex);
			this.Controls.Add(this.btnOut);
			this.Controls.Add(this.cbSerialOut);
			this.Controls.Add(this.t_recv);
			this.Controls.Add(this.cbSerial);
			this.Controls.Add(this.btnopen);
			this.Name = "MainForm";
			this.Text = "printerhook";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.ResumeLayout(false);
			this.PerformLayout();
		}
		private System.Windows.Forms.TextBox t_recvhex;
		private System.Windows.Forms.Button btnOut;
		private System.Windows.Forms.ComboBox cbSerialOut;
		private System.Windows.Forms.TextBox t_recv;
		private System.Windows.Forms.ComboBox cbSerial;
		private System.Windows.Forms.Button btnopen;
		
	}
}
